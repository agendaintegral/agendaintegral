﻿/*
    Agenda Integral Taller Toyota
    Copyright (C) 2015 Serinfer S.L.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    In order to get the source code take a look at LICENSE.txt file included in this folder

*/

$(document).ready(function () {
    $('select.dropdown').dropdown();

    debugger;
    var idTaller = 0;
    var idEmpresa;
    if (sessionStorage.getItem('idEmpresa')) {
        idEmpresa = sessionStorage.getItem('idEmpresa');
    }
    var permisosRecepcion = false;
    var permisosMecanica = false;

    if(window.location.search === "") {
        sessionStorage.clear();
        alert("No ha entrado desde Serauto o ha refrescado la Agenda ");

        idEmpresa = 0;
    }

    function getUrlVars() {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    
    function getRol() {
       
        if (getUrlVars()["id"]) {
            idEmpresa = getUrlVars()["id"];
            sessionStorage.setItem('idEmpresa', idEmpresa);

            var username = getUrlVars()["username"];
            var password = getUrlVars()["password"];
			
            var json = "/Login?idempresa=" + idEmpresa + "&username=" + username
                + "&password=" + password;

            $.ajax({
                dataType: "json",
                url: json,
                type: "GET",
                async: false,
                data: { get_param: 'value' },
                success: function(data) {
                    result = data;
                    if (!result.CitasEnabled) {
                        document.getElementById('calendarRec').style.pointerEvents = 'none';
                    }

                    if (!result.TareasEnabled) {
                        document.getElementById('calendarMec').style.pointerEvents = 'none';
                    }

                    if (!result.TareasEnabled) {
                        document.getElementById('external-events').style.pointerEvents = 'none';
                    }

                    if (!result.Logged) {
                        alert("USUARIO NO EXISTE");
                        sessionStorage.clear();
                        window.location.href = "http://google.com";                   
                    }



                    $('#userLogin').text(result.title);

                    var icon = document.createElement("i");
                    icon.className = "icon user";

                    $('#userLogin').prepend(icon);

                    var clean_uri = location.protocol + "//" + location.host + location.pathname;

                    window.history.replaceState({}, document.title, clean_uri);
                   
                },
                error: function() {
                    alert('Error al cargar el rol del usuario');
                }
            });
        }
    }

    getRol();


    function initNombreFecha() {
        if (!sessionStorage.getItem('nombreTaller')) {
            $('#nombreTaller').text('Nombre del taller');
        } else {
            $('#nombreTaller').text(sessionStorage.getItem('nombreTaller'));
            $(".default.text").text(sessionStorage.getItem('nombreTaller'));
        }
    
        var fechaActual = moment();
        if ($('#calendarRec').fullCalendar('getDate').format('YYYY-MM-DD') != fechaActual.format('YYYY-MM-DD')) {
            $('.timeline').remove();
            $('#fechaTitulo').text($('#calendarRec').fullCalendar('getDate').format('DD/MM/YYYY'));
        } else {
            setTimelineRec(minHorario, maxHorario);
        }
    }

    function horaActualCabecera() {
        var fechaActual = moment();

        if ($('#calendarRec').fullCalendar('getDate').format('YYYY-MM-DD') == fechaActual.format('YYYY-MM-DD')) {
            $('#fechaTitulo').text(fechaActual.format('DD/MM/YYYY HH:mm:ss'));
        }
    }

     setInterval(function () {
            horaActualCabecera();
     }, 1000);
    

    function getTalleres() {

        $.getJSON("/Talleres?idempresa=" + idEmpresa, function (json) {
            var longitud = json.length;
            var div;

            for (var i = 0; i < longitud; i++) {
                var elem = document.createElement("OPTION");
                elem.className = "item";
                elem.setAttribute("data-value", i);
                elem.id = json[i].Id;
                elem.text = json[i].Nombre;

                div = document.createElement('div');
                div.className = div.className + "item";

                div.data = json[i];

                $('#listatalleres').append(elem);
            }
        });
    }
    getTalleres(); 

    function getIdTaller() {
        $('#listatalleres').change(function () {
            $('#loading').show();

            var select = document.getElementById("listatalleres");
            var option = select.options[select.selectedIndex];
            idTaller = option.id;
            sessionStorage.setItem('idTaller', idTaller);
            sessionStorage.setItem('nombreTaller', option.text);
            $('.fc-event.ui-draggable').remove();
            initTareasNoAsignadas(0);

            console.log(idTaller);
            $('#calendarRec').fullCalendar('destroy');
            $('#calendarRec').fullCalendar(initOpcionesRecepcion(sessionStorage.getItem('idTaller')));
            $('#calendarRec').fullCalendar('render');

            $('#calendarMec').fullCalendar('destroy');
            $('#calendarMec').fullCalendar(initOpcionesMecanica(sessionStorage.getItem('idTaller')));
            $('#calendarMec').fullCalendar('render');


            initBotonesComunes();
            contarRecepcionistasMecanicos();
        });   
    }
    getIdTaller();

    function getHorario(idTallerAux) {
        var min = "09:00:00";
        var max = "21:00:00";
        var toret = [];
        var json = "/HorarioTaller?id=" + idTallerAux;
        $.ajax({
            dataType: "json",
            url: json,
            type: "GET",
            async: false,
            data: { get_param: 'value' },
            success: function (data) {
                result = data;
                if (result.minTime != null && result.maxTime != null) {
                    min = result.minTime;
                    max = result.maxTime;
                }
                toret.push(min);
                toret.push(max);
            },
            error: function () {
                alert('Error al cargar el horario del taller');
            }
        });

        return toret;
    }


    getHorario(sessionStorage.getItem('idTaller'));

    var minHorario = parseInt(getHorario(sessionStorage.getItem('idTaller'))[0]);
    var maxHorario = parseInt(getHorario(sessionStorage.getItem('idTaller'))[1]);
        
    function setTimelineRec(minHorario, maxHorario) {
        var parentDivAux = jQuery(".fc-time-area.fc-widget-content").children().children().children().children();
        var parentDiv = jQuery();

        var mod = 0;
        if ($('#contenidoRecepcionistas').is(':hidden')) {
            mod = 2;
        }

        for (var i = mod; i < parentDivAux.length; i++) {
            if (parentDivAux[i].className === "fc-bg" && parentDivAux[i].clientWidth != 0) {
                parentDiv.push(parentDivAux[i]);
            }
        }
        
        var timeline = parentDiv.children(".timeline").children();
        if (timeline.length == 0) { 
            timeline = jQuery("<hr>").addClass("timeline");

            var segundosHoraMin = minHorario * 3600;
            var segundosHoras = (maxHorario-minHorario) * 3600;

            var ancho = parentDiv.width();
			
            var curTime = new Date();
            var curSeconds = (curTime.getHours() * 60 * 60) + (curTime.getMinutes() * 60) + curTime.getSeconds() - segundosHoraMin;
            var percentOfDay = curSeconds / segundosHoras; 

            var margen = (percentOfDay * ancho);

            timeline.css({
                left: margen-4 + "px",
                height: "100%",
                width: 1 + "px"
            });

            parentDiv.prepend(timeline);
        }

    }

    var intervalo = null;
    
    function startIntervalo() {
        console.log("intervalo starteado");

        intervalo = setTimeout(function () {
            $('#calendarRec').fullCalendar('refetchEvents');
            $('#calendarMec').fullCalendar('refetchEvents');
            var fechaCalendar = $('#calendarRec').fullCalendar('getDate');
            var fechaActual = moment();
            if (fechaCalendar.format('YYYY-MM-DD') === fechaActual.format('YYYY-MM-DD')) {
                $('.timeline').remove();
                setTimelineRec(minHorario, maxHorario);
            } else {
                $('.timeline').remove();
            }

            startIntervalo();

        }, 6500);
    }

    startIntervalo();

    function stopIntervalo() {
        console.log("intervalo parado");
        clearTimeout(intervalo);
    }

    $(window).on('resize', function () {
        var fechaCalendar = $('#calendarRec').fullCalendar('getDate');
        var fechaActual = moment();
        if (fechaCalendar.format('YYYY-MM-DD') === fechaActual.format('YYYY-MM-DD')) {
            $('.timeline').remove();
            setTimelineRec(minHorario, maxHorario);
        } else {
            $('.timeline').remove();
        }
    });

    $('#calendarRec').fullCalendar(
        initOpcionesRecepcion(sessionStorage.getItem('idTaller'))
    );

    function dragEventosExternos() {
        $('#external-events .fc-event').each(function() {
           
            $(this).data('event', {
                title: $.trim($(this).data('title')),
                description: $.trim($(this).data('descripcion')),
                allDay: false,
                idOr: $.trim($(this).data('idOr')),
                añoOr: $.trim($(this).data('añoOr')),
                idCita: $.trim($(this).data('idCita')),
                idOrAveria: $.trim($(this).data('idORAveria')),
                nombre: $.trim($(this).data('nombre')),
                apellidos: $.trim($(this).data('apellidos')),
                matricula: $.trim($(this).data('matricula')),
                duracionHoras: $.trim($(this).data('duration'))
            });

            $(this).draggable({
                start: function() {
                    $('#calendarMec').fullCalendar('startDrag');
                    console.log("iniciado drag");
                    stopIntervalo();
                },
                stop: function() {
                    console.log("stop drag, drop ahora");
                    var res = $('#calendarMec').fullCalendar('stopDrag'); 

                    startIntervalo();
                },

                zIndex: 999,
                revert: true,
                revertDuration: 0 
            });

        });

       
    }

    var tooltips;

    function initOpcionesRecepcion(idTallerAux) {
        var opciones = {
            now: new Date(),
            lazyFetching: false,
            
            editable: true, 
            minTime: getHorario(idTallerAux)[0],
            maxTime: getHorario(idTallerAux)[1],
            resourceAreaWidth: "15%",
            eventDurationEditable: false,
            height: 'auto',
            slotDuration: "00:15",
            axisFormat: 'HH:mm',
            timeFormat: 'HH:mm',
            slotWidth: "50%",
            columnFormat: 'ddd D/M',
            scrollTime: '00:00',
            header: {
                left: '',
                center: '',
                right: ''
            },
            defaultView: 'timelineTwoDays',
            views: {
                timelineTwoDays: {
                    type: 'timeline',
                    duration: { days: 1 }
                }
            },
            resourceLabelText: 'Recepcionistas',
 
            resources: '/Recepcionistas?idempresa=' + idEmpresa + '&idtaller=' + sessionStorage.getItem('idTaller'),

            //Cargamos las citas y el horario no disponible
            eventSources: [
                {
                    url: '/Citas?idempresa=' + idEmpresa + '&idtaller=' + sessionStorage.getItem('idTaller')
                },
                {
                    url: ' /HorariosRecepcionistas?idempresa=' + idEmpresa + '&idtaller=' + sessionStorage.getItem('idTaller'),
                }
            ],
            eventOverlap: false,
            viewRender: function (view) {
                try {
                } catch (err) { }
            },
            loading: function(bool) {
                if (!bool) {
                    var noDisponibles = document.getElementsByClassName('horarioNoDisponible');

                    for (var i = 0; i < noDisponibles.length; i++) {
                        noDisponibles[i].style.height = noDisponibles[i].parentNode.style.height;
                    }
                } else {
                    var noDisponibles = document.getElementsByClassName('horarioNoDisponible');

                    for (var i = 0; i < noDisponibles.length; i++) {
                        noDisponibles[i].style.height = noDisponibles[i].parentNode.style.height;
                    }
                }
            },
            eventDragStart: function(event, jsEvent, ui, view) {
                stopIntervalo();     
            },
            eventDragStop: function (event, jsEvent, ui, view) {
                if (isEventOverDivManhanaRecepcion(jsEvent.clientX, jsEvent.clientY)) {
                    var fechaActual = moment().startOf('hour');
                    var endAux = moment().startOf('hour');
                    var diaActual = fechaActual.format('d');

                    if ($('#calendarRec').fullCalendar('getDate').format('YYYY-MM-DD') == fechaActual.format('YYYY-MM-DD')) {

                        if (diaActual === "5") {
                            fechaActual.add(3, 'days').hour(9).minute(0);
                            endAux.add(3, 'days').hour(9).minute(0);
                        } else {
                            fechaActual.add(1, 'days').hour(9).minute(0);
                            endAux.add(1, 'days').hour(9).minute(0);
                        }
                        var diff = event.end - event.start;
                        event.start = fechaActual;
                        event.end = endAux.add(15, 'minutes');

                        var json = "/Citas?idempresa=" + idEmpresa + '&idtaller=' + sessionStorage.getItem('idTaller') +
                             "&idcita=" + event.id.substring(0, event.id.length - 1) + "&idempleado=" + event.resourceId + "&start=" + event.start.format('YYYY-MM-DDTHH:mm:ss')
                             + "&end=" + event.end.format('YYYY-MM-DDTHH:mm:ss') + '&tipo=' + event.tipo;


                        $.ajax({
                            dataType: "json",
                            url: json,
                            async: false,
                            type: "PUT",
                            success: function (data) {
                                console.log("Evento puteado");
                                $('#calendarRec').fullCalendar('removeEvents', event);
                            },
                            error: function () {
                                alert('Error al mover la cita del recepcionsita');
                            }
                        });
                    } else {
                        alert("No puedes mover la cita");
                    }
                }
            },
            eventDrop: function (event, delta, revertFunc) {

                var json = "/Citas?idempresa=" + idEmpresa + '&idtaller=' + sessionStorage.getItem('idTaller') + "&idcita=" + event.id.substring(0, event.id.length - 1) + "&idempleado=" + event.resourceId + '&start=' + event.start.format() + "&end=" + event.end.format() + "&tipo=" + event.tipo;

                var eventos = $('#calendarRec').fullCalendar('clientEvents');
                var eventsAux = [];
                var eventsMecAux = [];

                var mayorSalida = 0;
                var menorEntrada = moment(131878187600406);
                var tieneOR = false;
                var tieneCita = false;

                for (var i = 0; i < eventos.length; i++) {
                    if (eventos[i].id !== 0 &&  eventos[i].id.substring(0, eventos[i].id.length - 1) == event.id.substring(0, event.id.length - 1)
                        && eventos[i].id !== event.id ) {
                        eventsAux.push(eventos[i]);
                        tieneCita = true;
                    }
                }

                eventos = $('#calendarMec').fullCalendar('clientEvents');
                for (var i = 0; i < eventos.length; i++) {
                    if (eventos[i].id != 0 && eventos[i].idCita == event.id.substring(0, event.id.length - 1)) {
                        eventsMecAux.push(eventos[i]);

                        tieneOR = true;
                    }
                }

                for (var i = 0; i < eventsMecAux.length; i++) {
                    if (eventsMecAux[i].start < menorEntrada) {
                        menorEntrada = eventsMecAux[i].start;
                    }

                    if (eventsMecAux[i].end > mayorSalida) {
                        mayorSalida = eventsMecAux[i].end;
                    }
                }

                if (tieneOR) {
                    if (event.start > (menorEntrada - 15*60*1000) && event.tipo == "R") {
                        revertFunc();
                    }else if (event.end < (mayorSalida + 15*60*1000) && event.tipo == "E") {
                        revertFunc();
                    } else {

                        $.ajax({
                            dataType: "json",
                            url: json,
                            crossDomain: true,
                            type: "PUT",
                            success: function () {
                                console.log("cita actualizada");
                                $('#calendarRec').fullCalendar('refetchEvents');
                            },
                            error: function () {
                                alert('Error al actualizar la cita');
                            }
                        });
                    }

                }else if (tieneCita) {
                    if (event.start > eventsAux[0].start && event.tipo == "R") {
                        revertFunc();
                    } else if (event.start < eventsAux[0].start && event.tipo == "E") {
                        revertFunc();

                    } else {

                        $.ajax({
                            dataType: "json",
                            url: json,
                            crossDomain: true,
                            type: "PUT",
                            success: function() {
                                console.log("cita actualizada");
                                $('#calendarRec').fullCalendar('refetchEvents');
                            },
                            error: function() {
                                alert('Error al actualizar la cita');
                            }
                        });
                    }
                } else {
                    $.ajax({
                        dataType: "json",
                        url: json,
                        crossDomain: true,
                        type: "PUT",
                        success: function () {
                            console.log("cita actualizada");
                            $('#calendarRec').fullCalendar('refetchEvents');
                        },
                        error: function () {
                            alert('Error al actualizar la cita');
                        }
                    });
                }

                startIntervalo();  

            },
            eventAfterAllRender: function(view) {
                initNombreFecha();
                tamañoContenedorRec();
            },
            eventClick: function (calEvent, jsEvent, view) {

                if (calEvent.id !== 0) {
                    fillInfoCita(calEvent);
                   

                    stopIntervalo();

                    tooltips = $(this).qtip({ 
                        content: {
                            text: $('#infoTabla').focus()
                        },
                        show: {
                            event: 'click',
                            effect: function() {
                                $(this).show(500);
                                stopIntervalo();
                                $('#infoTabla').focus();
                            }
                        },
                        position : {                         
                            adjust : {
                                scroll : false
                            }
                        },
                        hide: {
                            event: 'unfocus click',
                            target: $('#infoCerrar'),
                            effect: function() {
                                $(this).hide(500);
                                startIntervalo();
                                cerrarInfoClearClasesRec(calEvent);
                            }
                        },
                        api: {
                            onHide: cerrarInfoClearClasesRec(calEvent)                          
                        }

                    });

                    var api = tooltips.qtip('api').show();

                    var eventos = $('#calendarRec').fullCalendar('clientEvents');
                    var eventosSeleccionados = [];

                    for (var i = 0; i < eventos.length; i++) {
                        if (eventos[i].id !== 0 && eventos[i].id.substring(0, eventos[i].id.length - 1) == calEvent.id.substring(0, calEvent.id.length - 1)) {
                            eventosSeleccionados.push(eventos[i]);
                        }
                    }

                    for (var i = 0; i < eventosSeleccionados.length; i++) {
                        var clases = eventosSeleccionados[i].className;
                        clases.push("seleccionado");

                        for (var j = 0; j < clases.length; j++) {
                            eventosSeleccionados[i].className = clases;
                        }

                        $('#calendarRec').fullCalendar('updateEvent', eventosSeleccionados[i]);                        
                    }

                    eventosSeleccionados = [];

                    
                    eventos = $('#calendarMec').fullCalendar('clientEvents');

                    for (var i = 0; i < eventos.length; i++) {
                        if (eventos[i].id !== 0 && eventos[i].idCita == calEvent.id.substring(0, calEvent.id.length - 1)) {
                            eventosSeleccionados.push(eventos[i]);
                        }
                    }

                    for (var i = 0; i < eventosSeleccionados.length; i++) {
                        var clases = eventosSeleccionados[i].className;
                        clases.push("seleccionado");  

                        eventosSeleccionados[i].className = " seleccionado ";
                        for (var j = 0; j < clases.length; j++) {
                            eventosSeleccionados[i].className = clases;
                        }

                        $('#calendarMec').fullCalendar('updateEvent', eventosSeleccionados[i]);
                    }

                }               
            }
        }

        return opciones;
    }


    function getRecepcionistas() {
        var json = '/Recepcionistas?idempresa=' + idEmpresa + '&idtaller=' + sessionStorage.getItem('idTaller');

        var recepcionistas = [];

        $.ajax({
            dataType: "json",
            url: json,
            type: "GET",
            async: false,
            data: { get_param: 'value' },
            success: function (data) {
                result = data;
                if (result != null) {
                    recepcionistas = result;
                }

            },
            error: function () {
                alert('Error al cargar el horario del taller');
            }
        });

        return recepcionistas;

    }
    
    function pierdeFocoInfoRec() {
        $('#infoTabla').focusout(function() {
            $('#infoDescripcionRecepcion').text("");
            $('#calendarRec').fullCalendar('refetchEvents');
            $('#calendarMec').fullCalendar('refetchEvents');

        });
    }

    pierdeFocoInfoRec();

    function pierdeFocoInfoMec() {
        $('#infoMecanica').focusout(function () {
            $('#infoDescripcionMecanica').text("");
            $('#calendarRec').fullCalendar('refetchEvents');
            $('#calendarMec').fullCalendar('refetchEvents');

        });
    }

    pierdeFocoInfoMec();

    function getNoDisponibleRecepcionistas() {
        var recepcionistas = getRecepcionistas();
        var eventos = [];
        for (var i = 0; i < recepcionistas.length; i++) {

            var json = '/HorarioRecepcionista?idempresa=' + idEmpresa +
                '&idrecepcionista=' + recepcionistas[i].id + '&fecha=' + moment().format('YYYY-MM-DD');


            $.ajax({
                dataType: "json",
                url: json,
                async: false,
                type: "GET",
                success: function (data) {
                    result = data
                    if (result != null) {
                        for (var i = 0; i < result.length; i++) {
                            eventos.push(result[i]);
                        }
                    }
                },
                error: function () {
                    console.log('Error con el horario personal de los mecánicos');
                }
            });
        }

        return eventos;
    }

    function getMecanicos() {
        var json = 'http://si-desarrollo31/restServiceAgendaTaller/Mecanicos?idempresa=' + idEmpresa + '&idtaller=' + sessionStorage.getItem('idTaller');

        var mecanicos = [];

        $.ajax({
            dataType: "json",
            url: json,
            type: "GET",
            async: false,
            data: { get_param: 'value' },
            success: function (data) {
                result = data;
                if (result != null) {
                    mecanicos = result;
                }

            },
            error: function () {
                alert('Error al cargar el horario del taller');
            }
        });

        return mecanicos;

    }

    function fillInfoCita(event) {
        if (event.matricula.length === 0) {
            $('#infoMatricula').text("Sin matrícula");
        } else {
            $('#infoMatricula').text(event.matricula);
        }
        $('#infoTiempo').text("15 min");
        $('#infoNombre').text(event.cliente);

        $('#inforOrCita').text("Cita " + event.id.substring(0, event.id.length - 1));
        if (event.tipo == "R") {
            $('#infoAveria').text("Recepción");
        } else {
            $('#infoAveria').text("Entrega");
        }

        if (!event.telefono) {
            $('#infoTelefono').text("Sin teléfono");
        } else {
            $('#infoTelefono').text(event.telefono);
        }

        if (event.descripcion) {
            var descripciones = event.descripcion.split("$");

            for (var i = 0; i < descripciones.length; i++) {
                var br = document.createElement("br");
                $('#infoDescripcionRecepcion').prepend(br);
                $('#infoDescripcionRecepcion').text($('#infoDescripcionRecepcion').text() + " " + descripciones[i]);

            }
        }

    }

    function fillInfoMecanica(event) {
        if (!event.matricula) {
            $('#infoMatriculaMecanica').text("Sin matrícula");
        } else {
            $('#infoMatriculaMecanica').text(event.matricula);
        }

        $('#infoTiempoMecanica').text(Math.round(((event.end - event.start)/1000)/60) + " min");

        if (event.nombre && event.apellidos) {
            $('#infoNombreMecanica').text(event.nombre + " " + event.apellidos);
        } else {
            $('#infoNombreMecanica').text("Sin nombre - apellidos");
        }
        $('#infoOrMecanica').text("OR " + event.idOR + "/" + event.añoOR);
        $('#infoAveriaMecanica').text("Avería " + event.idORAveria);

        if (!event.Telefono) {
            $('#infoTelefonoMecanica').text("Sin teléfono"); 
        } else {
            $('#infoTelefonoMecanica').text(event.Telefono);
        }


        if (event.idOR === 0) {
            $('#infoDescripcionMecanica').text("Trabajo interno del taller");
        } else {
            $('#infoDescripcionMecanica').text(event.descripcion);
        }
    }

    var cerrarInfo = document.getElementById('infoCerrar');
    function cerrarInfoClearClasesRec(event) {
        cerrarInfo.onclick = function () {
            $('#infoDescripcionRecepcion').text("");

            var eventos = $('#calendarRec').fullCalendar('clientEvents');

            var eventosAux = [];
            eventosAux.push(event);

            for (var i = 0; i < eventos.length; i++) {
                if (eventos[i].id !=0 && event.id.substring(0, event.id.length - 1) == eventos[i].id.substring(0, eventos[i].id.length - 1)
                    && event.id !== eventos[i].id) {
                    eventosAux.push(eventos[i]);
                }
            }

         
            for (var j = 0; j < eventosAux.length; j++) {
            
                var clases = eventosAux[j].className;
                console.log(clases);

                for (var k = 0; k < clases.length; k++) {
                    if (clases[k] == "seleccionado") {
                        clases.splice(k, 1);
                    }
                }

                eventosAux[j].className = clases;

                $('#calendarRec').fullCalendar('updateEvent', eventosAux[j]);
            }

            eventos = $('#calendarMec').fullCalendar('clientEvents');
            var eventosAux = [];

            for (var i = 0; i < eventos.length; i++) {
                if (eventos[i].id != 0 && event.id.substring(0, event.id.length - 1) == eventos[i].idCita
                    && eventos[i].tipo != "flecha")
                {
                    eventosAux.push(eventos[i]);
                }
            }

            for (var j = 0; j < eventosAux.length; j++) {

                var clases = eventosAux[j].className;
                console.log(clases);

                for (var k = 0; k < clases.length; k++) {
                    if (clases[k] == "seleccionado") {
                        clases.splice(k, 1);
                    }
                }

                eventosAux[j].className = clases;

                $('#calendarMec').fullCalendar('updateEvent', eventosAux[j]);
            }
        }
    }

    var cerrarInfoMecanica = document.getElementById('infoCerrarMecanica');
    function cerrarInfoClearClasesMec(event) {
        cerrarInfoMecanica.onclick = function () {
            
            var eventos = $('#calendarMec').fullCalendar('clientEvents');

            var eventosAux = [];
            eventosAux.push(event);

            for (var i = 0; i < eventos.length; i++) {
                if (eventos[i].id !== 0 && eventos[i].idOR == event.idOR && eventos[i].idORAveria == event.idORAveria
                    && eventos[i].añoOR == event.añoOR ) {
                    eventosAux.push(eventos[i]);
                }
            }

            for (var j = 0; j < eventosAux.length; j++) {

                var clases = eventosAux[j].className;

                for (var k = 0; k < clases.length; k++) {
                    if (clases[k] == "seleccionado") {
                        clases.splice(k, 1);
                    }
                }

                eventosAux[j].className = clases;

                $('#calendarMec').fullCalendar('updateEvent', eventosAux[j]);
            }

            eventos = $('#calendarRec').fullCalendar('clientEvents');
            eventosAux = [];

            for (var i = 0; i < eventos.length; i++) {
                if (eventos[i].id !== 0 && eventos[i].id.substring(0, eventos[i].id.length - 1) == event.idCita 
                    && event.tipo != "flecha") {
                    eventosAux.push(eventos[i]);
                }
            }

            for (var j = 0; j < eventosAux.length; j++) {

                var clases = eventosAux[j].className;

                for (var k = 0; k < clases.length; k++) {
                    if (clases[k] == "seleccionado") {
                        clases.splice(k, 1);
                    }
                }

                eventosAux[j].className = clases;

                $('#calendarRec').fullCalendar('updateEvent', eventosAux[j]);
            }


            
        }

    }

    function contarRecepcionistasMecanicos() {
        var json = "/Recepcionistas?idempresa=" + idEmpresa + '&idtaller=' + sessionStorage.getItem('idTaller');

        $.ajax({
            dataType: "json",
            url: json,
            type: "GET",
            data: { get_param: 'value' },
            success: function (data) {
                result = data;
                if (result != null) {
                    $('#numRecepcionistas').text(result.length);
                }
            },
            error: function () {
                alert('Error al cargar el horario del taller');
            }
        });

        json = "/Mecanicos?idempresa=" + idEmpresa + '&idtaller=' + sessionStorage.getItem('idTaller');

        $.ajax({
            dataType: "json",
            url: json,
            type: "GET",
            data: { get_param: 'value' },
            success: function (data) {
                result = data;
                if (result != null) {                   
                    $('#numMecanicos').text(result.length);
                }
            },
            error: function () {
                alert('Error al cargar el horario del taller');
            }
        });
    }

    contarRecepcionistasMecanicos();

    function desplegarColapsarRecepcion() {
        if ($('#contenidoRecepci' +
            'onistas').css('display') == 'none') {
            $('#contenidoRecepcionistas').slideDown();
            $('#iconoRecepcionistas').removeClass('icon angle down').addClass('icon angle up');
        } else {
            $('#contenidoRecepcionistas').slideUp();
            $('#iconoRecepcionistas').removeClass('icon angle up').addClass('icon angle down');
        }
    }
     
    var barraRecepcionistas = document.getElementById('barraRecepcionistas');
    
    barraRecepcionistas.onclick = function() {
        desplegarColapsarRecepcion();
    }

    function desplegarColapsarMecanica() {
        if ($('#contenidoMecanicos').css('display') == 'none') {
            $('#contenidoMecanicos').slideDown();
            $('#iconoMecanicos').removeClass('icon angle down').addClass('icon angle up');
        } else { 
            $('#contenidoMecanicos').slideUp();
            $('#iconoMecanicos').removeClass('icon angle up').addClass('icon angle down');
        }
    }

    var barraMecanicos = document.getElementById('barraMecanicos');

    barraMecanicos.onclick = function () {
        desplegarColapsarMecanica();
    }

    var iconoTareasSinAsignar = document.getElementById('iconoTareasSinAsignar');
    var labelTareasSinAsignar = document.getElementById('labelTareasSinAsignar');

    function desplegarColapsarTareasSinAsignar() {
        if ($('#externalevents').css('display') == 'none') {
            $('#externalevents').slideDown();
            $('#iconoTareasSinAsignar').removeClass('chevron circle down icon').addClass('chevron circle up icon');
        } else {
            $('#externalevents').slideUp();
            $('#iconoTareasSinAsignar').removeClass('chevron circle up icon').addClass('chevron circle down icon');
        }
    }

    iconoTareasSinAsignar.onclick = function() {
        desplegarColapsarTareasSinAsignar();
    }

    labelTareasSinAsignar.onclick = function () {
        desplegarColapsarTareasSinAsignar();
    }


    var isEventOverDiv = function (x, y) {
        var scroll = $(document).scrollTop();
        var coordenadaX = x;
        var coordenadaY = y + scroll;

        var external_events = $('#external-events');

        var offset = external_events.offset();
        offset.right = external_events.width() + offset.left;
        offset.bottom = offset.top + external_events.height();

        if (coordenadaX >= offset.left
            && coordenadaY >= offset.top
            && coordenadaX <= offset.right
            && coordenadaY <= offset.bottom) { return true; }
        return false;
    }

    var isEventOverDivManhanaMecanica = function (x, y) {
        var scroll = $(document).scrollTop();
        var coordenadaX = x;
        var coordenadaY = y + scroll;

        var contenedorManhana = $('#contenedorMecanica');

        var offset = contenedorManhana.offset();
        offset.right = contenedorManhana.width() + offset.left;
        offset.bottom = offset.top + contenedorManhana.height();

        if (coordenadaX >= offset.left
            && coordenadaY >= offset.top
            && coordenadaX <= offset.right
            && coordenadaY <= offset.bottom) { return true; }
        return false;
    }

    var isEventOverDivManhanaRecepcion = function (x, y) {
        var scroll = $(document).scrollTop();
        var coordenadaX = x;
        var coordenadaY = y + scroll;

        var contenedorManhana = $('#contenedorRecepcion');

        var offset = contenedorManhana.offset();
        offset.right = contenedorManhana.width() + offset.left;
        offset.bottom = offset.top + contenedorManhana.height();

        if (coordenadaX >= offset.left
            && coordenadaY >= offset.top
            && coordenadaX <= offset.right
            && coordenadaY <= offset.bottom) { return true; }
        return false;
    }

    function initTareasNoAsignadas(page) {
        var json = "/TareasSinAsignar?idempresa=" + idEmpresa + '&idtaller=' + sessionStorage.getItem('idTaller');
        var duracion = 60;
        var div;
        var divOr;
        var divAveria;
        var labelOr;
        var labelTiempo;
        var labelAveriaTitle;
        var labelAveriaNum;

        var external = document.getElementById('external-events');
        var arrayToret = [];

        $.ajax({
            dataType: "json",
            url: json,
            type: "GET",
            async: false,

            data: { get_param: 'value' },
            success: function (data) {
                result = data;

                arrayToret = result;

                console.log("aqui se renderizan los 25 primeros cosos sin asignar");

                for (var i = result.length - (page*25 + 25); i < result.length-(page*25); i++) { //25 de momento, ya se hará un paginator
                    div = document.createElement('div');
                    div.className = div.className + "fc-event ui-draggable " + result[i].className;
               
                    if (result[i].duracionHoras != 0) {
                        jQuery.data(div, 'duration', moment.duration(result[i].duracionHoras, 'hour')); 
                    } else {
                        jQuery.data(div, 'duration', moment.duration(1, 'hour')); 
                    }

                    jQuery.data(div, 'idOr', result[i].idOR);
                    jQuery.data(div, 'añoOr', result[i].añoOR);
                    jQuery.data(div, 'idORAveria', result[i].idORAveria);
                    jQuery.data(div, 'apellidos', result[i].apellidos);
                    jQuery.data(div, 'nombre', result[i].nombre);
                    jQuery.data(div, 'matricula', result[i].matricula);
                    jQuery.data(div, 'description', result[i].descripcion);
                    jQuery.data(div, 'idCita', result[i].idCita);
                    jQuery.data(div, 'title', result[i].idOR);

                    divOr = document.createElement('div');
                    divOr.className += "or";
                    labelOr = document.createElement('label');
                    labelOr.className = "lblOr";
                    divOr.appendChild(labelOr);

                    labelOr.textContent = 'OR ' + result[i].idOR;

                    labelTiempo = document.createElement('label');
                    labelTiempo.className = "lblTiempo";
                    if (result[i].duracionHoras == 0) {
                        labelTiempo.textContent = 1 * 60 + " min";
                    } else {
                        labelTiempo.textContent = result[i].duracionHoras * 60 + " min";
                    }
                    divOr.appendChild(labelTiempo);

                    div.appendChild(divOr);

                    divAveria = document.createElement('div');
                    divAveria.className += "averia";
                    labelAveriaTitle = document.createElement('label');
                    labelAveriaTitle.className = "txtAveria";
                   
                    labelAveriaTitle.textContent = "AVERÍA";
                    divAveria.appendChild(labelAveriaTitle);

                    labelAveriaNum = document.createElement('label');
                    labelAveriaNum.className = "lblAveria";
                    labelAveriaNum.textContent = result[i].idORAveria;

                    divAveria.appendChild(labelAveriaNum);

                    div.appendChild(divAveria);
                   
                    external.appendChild(div);
                }

                $('.pagination').jqPagination({
                    max_page: parseInt(result.length / 25 + 1),
                    page_string: "Página {current_page} de {max_page}",
                    paged: function (page) {
                        console.log(page);
                        $('.fc-event.ui-draggable').remove();
                        initTareasNoAsignadas(page - 1);
                    }
                });

                dragEventosExternos();

            },
            error: function () {
                console.log('Error al cargar las averías no asignadas');
            }
        });

        return arrayToret;
    }


    var arraySinAsingar = initTareasNoAsignadas(0);


    $('.pagination').jqPagination({
        max_page: parseInt(arraySinAsingar.length / 25 + 1),
        page_string: "Página {current_page} de {max_page}",
        paged: function (page) {
            console.log(page);
            $('.fc-event.ui-draggable').remove();
            initTareasNoAsignadas(page - 1);
        }
    });

    var refrescar = document.getElementById('botonRefrescar');

    refrescar.onclick = function () {
        $('.fc-event.ui-draggable').remove();
        initTareasNoAsignadas(0);

        $('.pagination').jqPagination('option', { current_page: 1 });

    }

    function initBotonesComunes() {
        $('.fc-today-button').click(function() {
            $('#calendarMec').fullCalendar('today');
            setTimelineRec(minHorario, maxHorario);
        });

        $('.fc-next-button').click(function() {
            $('#calendarMec').fullCalendar('next');
            $('.timeline').remove();

            if ($('.fc-today-button').attr('disabled') == "disabled") {
                setTimelineRec(minHorario, maxHorario);
            }

        });

        $('.fc-prev-button').click(function() {
            $('#calendarMec').fullCalendar('prev');
            $('.timeline').remove();

            if ($('.fc-today-button').attr('disabled') == "disabled") {
                setTimelineRec(minHorario, maxHorario);
            }
        });
    }

    initBotonesComunes();


    $('#calendarMec').fullCalendar(initOpcionesMecanica());

    function initOpcionesMecanica() {
        var opciones = {
            now: new Date(),
            editable: true, 
            minTime: "09:00",
            maxTime: "21:00",
            resourceAreaWidth: "15%",
            eventDurationEditable: true,
            eventOverlap: true,
            height: 'auto',
            slotDuration: "00:15",
            slotWidth: "50%",
            slotEventOverlap: true,
            scrollTime: '00:00', 
            droppable: true,
            dragRevertDuration: 0,
            header: {
                left: '',
                right: ''
            },
            defaultView: 'timelineTwoDays',
            views: {
                timelineTwoDays: {
                    type: 'timeline',
                    duration: { days: 1 }
                }
            },
            resourceLabelText: 'Mecánicos',
            resources: '/Mecanicos?idempresa=' + idEmpresa + '&idtaller=' + sessionStorage.getItem('idTaller'),
            eventSources: [
            {
                url: '/Tareas?idempresa=' + idEmpresa + '&idtaller=' + sessionStorage.getItem('idTaller')
            },
            {
                url: '/TareasEnCurso?idempresa=' + idEmpresa + '&idtaller=' + sessionStorage.getItem('idTaller')
            },
            {
                url: ' /HorariosMecanicos?idempresa=' + idEmpresa + '&idtaller=' + sessionStorage.getItem('idTaller'),
            }
            ],
            viewRender: function (view) {
                try {
                    //setTimelineRec();
                } catch (err) { }
            },

            drop: function (date, ui) {
                var originalEventObject = $(this).data("event");
                $(this).remove();

                postearEventoDropeado(originalEventObject);            
            },
            eventDragStart: function(event, jsEvent, ui, view) {
                stopIntervalo(); 
            },
            eventDragStop: function (event, jsEvent, ui, view) {
                var div;
                var divOr;
                var divAveria;
                var labelOr;
                var labelTiempo;
                var labelAveriaTitle;
                var labelAveriaNum;

                var external = document.getElementById('external-events');
               
                if (isEventOverDiv(jsEvent.clientX, jsEvent.clientY)) {
           
                    var tareas = $('#calendarMec').fullCalendar('clientEvents');
                    var tareasParaBorrar = [];
                    for (var i = 0; i < tareas.length; i++) {
                        if (tareas[i].tipo !== "flecha" && tareas[i].idOR == event.idOR && tareas[i].añoOR == event.añoOR
                            && tareas[i].idORAveria == event.idORAveria) {

                            $('#calendarMec').fullCalendar('removeEvents', tareas[i].id);


                            var json = "/Tarea?idempresa=" + idEmpresa + '&idtaller=' + sessionStorage.getItem('idTaller') +
                                "&idempleado=" + tareas[i].resourceId + "&idtarea=" + tareas[i].id;

                            $.ajax({
                                dataType: "json",
                                url: json,
                                type: "DELETE",
                                success: function (data) {
                                    console.log("Evento quitado del calendario");
                                },
                                error: function () {
                                    alert('Error al quitar la tarea del mecanico');
                                }
                            });
                        }
                    }
                                        
                    var json = "/Tarea?idempresa=" + idEmpresa + '&idtaller=' + sessionStorage.getItem('idTaller') +
                        "&idempleado=" + event.resourceId + "&idtarea=" + event.id;

                    $.ajax({
                        dataType: "json",
                        url: json,
                        type: "DELETE",
                        success: function (data) {
                            console.log("Evento quitado del calendario");
                        },
                        error: function () {
                            alert('Error al quitar la tarea del mecanico');
                        }
                    });

                    div = document.createElement('div');
                    div.className = div.className + "fc-event ui-draggable tareaSinAsignar";

                    if (event.duracionHoras != 0) {
                        jQuery.data(div, 'duration', moment.duration(event.duracionHoras, 'hour')); 
                    } else {
                        jQuery.data(div, 'duration', moment.duration(1, 'hour')); 
                    }

                    jQuery.data(div, 'idOr', event.idOR);
                    jQuery.data(div, 'añoOr', event.añoOR);
                    jQuery.data(div, 'idORAveria', event.idORAveria);
                    jQuery.data(div, 'apellidos', event.apellidos);
                    jQuery.data(div, 'nombre', event.nombre);
                    jQuery.data(div, 'matricula', event.matricula);
                    jQuery.data(div, 'description', event.descripcion);
                    jQuery.data(div, 'idCita', event.idCita);
                    jQuery.data(div, 'title', event.idOR);

                    divOr = document.createElement('div');
                    divOr.className += "or";
                    labelOr = document.createElement('label');
                    labelOr.className = "lblOr";
                    divOr.appendChild(labelOr);

                    labelOr.textContent = 'OR ' + event.idOR;

                    labelTiempo = document.createElement('label');
                    labelTiempo.className = "lblTiempo";
                    if (event.duracionHoras == 0) {
                        labelTiempo.textContent = 1 * 60 + " min";
                    } else {
                        labelTiempo.textContent = event.duracionHoras * 60 + " min";
                    }
                    divOr.appendChild(labelTiempo);

                    div.appendChild(divOr);

                    divAveria = document.createElement('div');
                    divAveria.className += "averia";
                    labelAveriaTitle = document.createElement('label');
                    labelAveriaTitle.className = "txtAveria";

                    labelAveriaTitle.textContent = "AVERÍA";
                    divAveria.appendChild(labelAveriaTitle);

                    labelAveriaNum = document.createElement('label');
                    labelAveriaNum.className = "lblAveria";
                    labelAveriaNum.textContent = event.idORAveria;

                    divAveria.appendChild(labelAveriaNum);

                    div.appendChild(divAveria);

                    external.appendChild(div);

                    dragEventosExternos();
                }

                if (isEventOverDivManhanaMecanica(jsEvent.clientX, jsEvent.clientY)) {
                    var fechaActual = moment().startOf('hour');
                    var diaActual = fechaActual.format('d');

                    if ($('#calendarRec').fullCalendar('getDate').format('YYYY-MM-DD') == fechaActual.format('YYYY-MM-DD')) {

                        if (diaActual === "5") {
                            fechaActual.add(3, 'days').hour(9).minute(0);
                        } else {
                            fechaActual.add(1, 'days').hour(9).minute(0);
                        }
                        var diff = event.end - event.start;
                        event.start = fechaActual;
                        event.end = moment(event.start+diff);

                        var json = "/Tarea?idempresa=" + idEmpresa + '&idtaller=' + sessionStorage.getItem('idTaller') +
                            "&idempleado=" + event.resourceId + "&idtarea=" + event.id + "&start=" + event.start.format('YYYY-MM-DDTHH:mm:ss') + "&end=" + event.end.format('YYYY-MM-DDTHH:mm:ss');

                        $('#calendarMec').fullCalendar('removeEvents', event);

                        $.ajax({
                            dataType: "json",
                            url: json,
                            type: "PUT",
                            success: function(data) {
                                console.log("Evento puteado");
                                
                            },
                            error: function() {
                                alert('Error al mover la tarea del mecanico');
                            }
                        });
                    } else {
                        alert("No puedes mover la tarea");
                    }
                }
            },
            loading: function (bool) {
                
                if (!bool) {
                    var tareas = $('#calendarMec').fullCalendar('clientEvents');
                    var cuadrados = [];
                    var flechas = [];

                    for (var i = 0; i < tareas.length; i++) {
                        if (tareas[i].tipo != "flecha") {
                            cuadrados.push(tareas[i]);
                        } else if (tareas[i].tipo == "flecha") {
                            flechas.push(tareas[i]);
                        }
                    }
                    for (var i = 0; i < flechas.length; i++) {
                        for (var j = 0; j < flechas.length; j++) {
                            if (i !== j) {
                                if (flechas[i].idOR == flechas[j].idOR && flechas[i].idORAveria == flechas[j].idORAveria
                                    && flechas[i].añoOR == flechas[j].añoOR
                                    && (flechas[i].end < flechas[j].start)) {
                                    flechas.splice(i, 1);
                                    if (i > 0) {
                                        i--;
                                    }
                                }
                            }
                        }
                    }

                    var finConRetraso = moment();

                    for (var i = 0; i < cuadrados.length; i++) {
                        for (var j = 0; j < flechas.length; j++) {
                            if (cuadrados[i].idOR == flechas[j].idOR && cuadrados[i].idORAveria == flechas[j].idORAveria
                                && cuadrados[i].añoOR == flechas[j].añoOR) {                             
                                var clases = cuadrados[i].className;
                                finConRetraso = cuadrados[i].end;
                                var clasesFlechas = flechas[j].className;

                                if (flechas[j].estado == "CO") {


                                    if (flechas[j].end > cuadrados[i].end) {
                                        clases.push("retraso");
                                        cuadrados[i].className = clases;
                                    }

                                    if (flechas[j].end >= (finConRetraso - (15 * 60 * 1000))) {
                                        clases.push("menos15");
                                        cuadrados[i].className = clases;
                                    }

                                    if (flechas[j].end < cuadrados[i].end) {
                                        clases.push("encurso");
                                        cuadrados[i].className = clases;
                                    }

                                } else if (flechas[j].estado == "FI") {


                                    if (flechas[j].end > cuadrados[i].end) {
                                        clasesFlechas.push("finalizada");
                                        flechas[j].className = clasesFlechas;

                                        clases.push("retraso");
                                        cuadrados[i].className = clases;
                                    }

                                    if (flechas[j].end < cuadrados[i].end) {
                                        clasesFlechas.push("finalizada");
                                        flechas[j].className = clasesFlechas;

                                        clases.push("encurso");
                                        cuadrados[i].className = clases;
                                    }


                                } else {
                                    clasesFlechas.push("interrumpida");
                                    flechas[j].className = clasesFlechas;

                                    clases.push("interrumpido");
                                    cuadrados[i].className = clases;
                                }

                                $('#calendarMec').fullCalendar('updateEvent', cuadrados[i]);
                            }
                        }
                    }

                    $('#calendarMec').find('.fc-widget-content').not('.fc-minor').not('.fc-major').not('.fc-resource-area').not('.fc-time-area').addClass('lineaMecanico')

                    $('.lineaMecanico').children().addClass('alturaMecanico');
                } 
            },
            eventDrop: function (event, delta, revertFunc) {               
                var citas = $('#calendarRec').fullCalendar('clientEvents');
                var tieneCitas = false;
                var entrada;
                var salida;

                for (var i = 0; i < citas.length; i++) {
                    if (citas[i].id != 0 && citas[i].id.substring(0, citas[i].id.length - 1) == event.idCita) {
                        if (citas[i].tipo == "R") {
                            entrada = citas[i].end;
                        } else {
                            salida = citas[i].start;
                        }

                        tieneCitas = true;
                    }
                }
                
                if (event.start < entrada) {
                    revertFunc();
                }else if (event.end > salida) {
                    revertFunc();
                } else {

                    if (event.end == null) {
                        event.end = event.start.add(1, 'hours');
                    }

                    var json = "/Tarea?idempresa=" + idEmpresa + '&idtaller=' + sessionStorage.getItem('idTaller') +
                        "&idempleado=" + event.resourceId + "&idtarea=" + event.id + "&start=" + event.start.format() + "&end=" + event.end.format();

                    $.ajax({
                        dataType: "json",
                        url: json,
                        type: "PUT",
                        success: function(data) {
                            console.log("Evento puteado");
                        },
                        error: function() {
                            alert('Error al mover la tarea del mecanico');
                        }
                    });
                }
                startIntervalo();  
            },
            eventResizeStart: function(event, jsEvent, ui, view) {
                stopIntervalo();  
            },
            eventResize: function (event, delta, revertFunc, jsEvent, ui, view) {
          
                var citas = $('#calendarRec').fullCalendar('clientEvents');
                var tieneCitas = false;
                var entrada;
                var salida;

                for (var i = 0; i < citas.length; i++) {
                    if (citas[i].id != 0 && citas[i].id.substring(0, citas[i].id.length - 1) == event.idCita) {
                        if (citas[i].tipo == "R") {
                            entrada = citas[i].end;
                        } else {
                            salida = citas[i].start;
                        }
                        tieneCitas = true;
                    }
                }
                
                if (event.start < entrada) {
                    revertFunc();
                    $('#calendarMec').fullCalendar('refetchEvents');

                    startIntervalo(); 
                }else if (event.end > salida) {
                    revertFunc();
                    $('#calendarMec').fullCalendar('refetchEvents');

                    startIntervalo(); 

                } else {
                    var json = "/Tarea?idempresa=" + idEmpresa + '&idtaller=' + sessionStorage.getItem('idTaller') +
                        "&idempleado=" + event.resourceId + "&idtarea=" + event.id + "&start=" + event.start.format() + "&end=" + event.end.format();

                    $.ajax({
                        dataType: "json",
                        url: json,
                        async: false,
                        type: "PUT",
                        success: function(data) {
                            console.log("Evento puteado");
                            $('#calendarMec').fullCalendar('refetchEvents');
                            startIntervalo(); 
                        },
                        error: function() {
                            alert('Error al cambiar el tamaño de la tarea del mecanico');
                        }
                    });
                }

            },
            eventAfterAllRender: function() {
                tamañoContenedorMec();
                $('#loading').hide();
            },      
            eventClick: function (calEvent, jsEvent, view) {
                var infoCortar = document.getElementById('infoCortar');

                if (calEvent.id !== 0) {
                    fillInfoMecanica(calEvent);
                    var tooltips = $(this).qtip({
                        content: {
                            text: $('#infoMecanica').focus(),
                        },
                        show: {
                            event: 'click',
                            effect: function() {
                                $(this).show(500);
                                stopIntervalo(); 
                                $('#infoMecanica').focus();
                            }
                        },
                        position: {
                            my: 'top left',  
                            adjust: {
                                scroll: false
                            }
                        },
                        hide: {
                            event: 'click unfocus scroll',
                            target: $('#infoCerrarMecanica'),
                            effect: function() {
                                $(this).hide(500);
                                startIntervalo(); 
                            }
                        },
                        api: {
                            show: {                                
                                event: infoCortar.onclick = function () {
                                    dividirEnTareas(calEvent);
                                }
                            },
                            onHide: cerrarInfoClearClasesMec(calEvent)                       
                        }
                    });

                    var api = tooltips.qtip('api').show(); 
                    if (calEvent.tipo != "flecha") {

                        var eventos = $('#calendarMec').fullCalendar('clientEvents');
                        var eventosSeleccionados = [];

                        for (var i = 0; i < eventos.length; i++) {
                            if (eventos[i].id !== 0 && eventos[i].idOR == calEvent.idOR && eventos[i].idORAveria == calEvent.idORAveria
                                && eventos[i].añoOR == calEvent.añoOR) {
                                eventosSeleccionados.push(eventos[i]);
                            }
                        }
                        for (var i = 0; i < eventosSeleccionados.length; i++) {
                            var clases = eventosSeleccionados[i].className;
                            clases.push("seleccionado");

                            eventosSeleccionados[i].className = " seleccionado ";
                            for (var j = 0; j < clases.length; j++) {
                                eventosSeleccionados[i].className = clases;
                            }

                            $('#calendarMec').fullCalendar('updateEvent', eventosSeleccionados[i]);
                        }
                        eventos = $('#calendarRec').fullCalendar('clientEvents');
                        eventosSeleccionados = [];

                        for (var i = 0; i < eventos.length; i++) {
                            if (eventos[i].id !== 0 && eventos[i].id.substring(0, eventos[i].id.length - 1) == calEvent.idCita) {
                                eventosSeleccionados.push(eventos[i]);
                            }
                        }
                        for (var i = 0; i < eventosSeleccionados.length; i++) {
                            var clases = eventosSeleccionados[i].className;
                            clases.push("seleccionado");

                            eventosSeleccionados[i].className = " seleccionado ";
                            for (var j = 0; j < clases.length; j++) {
                                eventosSeleccionados[i].className = clases;
                            }

                            $('#calendarRec').fullCalendar('updateEvent', eventosSeleccionados[i]);
                        }
                    }
                }
            }

        }

        return opciones;
    }

    function postearEventoDropeado(event) {
        var eventos = $('#calendarMec').fullCalendar('clientEvents');
        var eventosAux = [];
        for (var i = 0; i < eventos.length; i++) {
            if (eventos[i].idOr == event.idOr && eventos[i].idOrAveria == event.idOrAveria && eventos[i].añoOr == event.añoOr) {
                eventosAux.push(eventos[i]); 
            }
        }

        var json = "/Tarea?idempresa=" + idEmpresa + '&idtaller=' + sessionStorage.getItem('idTaller') +
            "&idempleado=" + eventosAux[0].resourceId + "&idor=" + eventosAux[0].idOr + "&añoor=" + eventosAux[0].añoOr
            + "&idoraveria=" + eventosAux[0].idOrAveria + '&start=' + eventosAux[0].start.format() + '&end=' + eventosAux[0].end.format();
        console.log(json);

        $.ajax({
            dataType: "json",
            url: json,
            type: "POST",
            async: false,
            success: function (data) {
                console.log("Evento posteado");
                eventosAux = [];
            },
            error: function () {
                console.log('Error al guardar la tarea');
            }
        });

        $('#calendarMec').fullCalendar('refetchEvents');
    }

    function dividirEnTareas(event) {
        var json = "/TareaDividir?idempresa=" + idEmpresa + '&idtaller=' + sessionStorage.getItem('idTaller') +
            "&idtarea=" + event.id;
        console.log(json);

        $.ajax({
            dataType: "json",
            url: json,
            type: "POST",

            success: function (data) {
                console.log("Tareas partidas");
            },
            error: function () {
                alert('Error al dividir en dos tareas');
            }
        });

        $('#calendarMec').fullCalendar('refetchEvents');
    }

    function tamañoContenedorRec() {
        var tamañoRec = $('#calendarRec').height();

        $('#contenedorRecepcion').height(tamañoRec);

    }

    function tamañoContenedorMec() {
        var tamañoMec = $('#calendarMec').height();
        $('#contenedorMecanica').height(tamañoMec);
    }

    $(function () {
        $("#datepicker").datepicker();

    });

    $(function ($) {
        $.datepicker.regional['es'] = {
            closeText: 'Cerrar',
            prevText: '<Ant',
            nextText: 'Sig>',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);
    });

    var logopicker = document.getElementById('logopicker');
    logopicker.addEventListener('click', function () {
        $("#logopicker").datepicker("show");
    });

    $('#datepicker').on('change', function (ev) {
        $('#calendarRec').fullCalendar('gotoDate', $('#datepicker').datepicker("getDate"));
        $('#calendarRec').fullCalendar('render');

        $('#calendarMec').fullCalendar('gotoDate', $('#datepicker').datepicker("getDate"));
        $('#calendarMec').fullCalendar('render');

        initNombreFecha();


    });
    $('#fechaTitulo').text($('#calendarRec').fullCalendar('getDate').format('DD/MM/YYYY'));


});
