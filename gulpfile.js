﻿/*
	instalar módulos gulp-concat, gulp-rename, etc. con 'npm install ...'
	luego lanzar el comando 'gulp' para compilar, o si se prefiere que
	escuche los cambios automáticamente y compile él sólo, 'gulp watch'
*/

var gulp = require('gulp'),
    gp_concat = require('gulp-concat'),
    gp_rename = require('gulp-rename'),
    gp_uglify = require('gulp-uglify'),
    gp_notify = require('gulp-notify'),
    minifyCSS = require('gulp-minify-css'),
    minifyHTML = require('gulp-minify-html');

gulp.task('js-agendas', function () {
    return gulp.src(['js/agendas.js'])
        .pipe(gp_rename('agendas.min.js'))
        .pipe(gp_uglify())
        .pipe(gulp.dest('js'));
});

gulp.task('default', [
    'js-agendas'
], function () { });

